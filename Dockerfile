FROM php:7.0-apache
RUN apt-get update && apt-get install -y \
		libfreetype6-dev \
		libjpeg62-turbo-dev \
		libpng-dev \
#sendmail \
		ssmtp \
		&& docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
		&& docker-php-ext-install -j$(nproc) gd mysqli \
		&& docker-php-ext-install pdo pdo_mysql
#ADD ./php.sh /opt/php.sh
#RUN chmod u+x /opt/php.sh

# config mails
COPY	php.ini				/usr/local/etc/php/
COPY	sendmail_path.ini	/usr/local/etc/php/conf.d/sendmail_path.ini
COPY	ssmtp.conf			/etc/ssmtp/ssmtp.conf
