<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 05/07/2018
 * Time: 18:57
 */

session_start();

require_once "lib/lib.php";
require_once "config/database.php";

if (!isset($_SESSION['logged_on_user'])) {
    echo (json_encode(array('done' => 'error')));
    exit();
}

$db = $DB;
$uid = $_SESSION['uid'];
$eid = $_POST['eid'];
$sql = "INSERT INTO likes (uid, eid) VALUES (:uid, :eid)";
$sth = $db->prepare($sql);
$sth->bindParam(':uid',$uid);
$sth->bindParam(':eid',$eid);
if ($sth->execute() == FALSE) {
    $sql = "DELETE FROM likes WHERE uid = :uid && eid = :eid";
    $sth = $db->prepare($sql);
    $sth->bindParam(':uid',$uid);
    $sth->bindParam(':eid',$eid);
    if ($sth->execute())
        echo(json_encode(array('done' => 'unlike')));
}
else
    echo(json_encode(array('done' => 'like')));
