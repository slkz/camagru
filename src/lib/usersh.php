<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 09/06/2018
 * Time: 04:06
 */

function session_add($name, $value)
{
    if (empty($value))
        $_SESSION[$name] = "";
    $_SESSION[$name] = $value;
}

function session_get($name)
{
    if (isset($_SESSION[$name]))
        return ($_SESSION[$name]);
}

function logout(){
    if ($_SESSION) {
        session_unset();
        session_destroy();
    }
}

function login_exist($db, $login)
{
    $sql = "SELECT id FROM users WHERE login = :login";
    $sth = $db->prepare($sql);
    $sth->execute(array(':login' => $login));
    if ($sth->rowCount())
        return TRUE;
    return FALSE;
}

function mail_exist($db, $mail)
{
    $sql = "SELECT id FROM users WHERE mail = :mail";
    $sth = $db->prepare($sql);
    $sth->execute(array(':mail' => $mail));
    if ($sth->rowCount())
        return TRUE;
    return FALSE;
}

function auth($db, $login, $passwd)
{
    // be sure to be connected to sql
    $hash = hash("whirlpool", $passwd);
    $sql = "SELECT id FROM users WHERE login = :login && passwd = :passwd && confirmed = 1";
    $sth = $db->prepare($sql);
    $sth->execute(array(':login' => $login, ':passwd' => $hash));
    return $sth->fetchColumn();
}

function confirm_account($db, $activateCode) {
    $sql = "UPDATE `users` SET `confirmed` = 1 WHERE `activateCode` = :activateCode";
    $sth = $db->prepare($sql);
    $sth->execute(array(':activateCode' => $activateCode));
    if ($sth->rowCount())
        return TRUE;
    return FALSE;
}

function get_uid($db, $username){
    // be sure to get connected to sql
    if (login_exist($db, $username)){
        $sql = "SELECT id FROM users WHERE login = :login";
        $sth = $db->prepare($sql);
        $sth->execute(array(':login' => $username));
        return $sth->fetchColumn(PDO::FETCH_ASSOC);
    }
    else
        return -1;
}

function get_username($db, $uid) {
    $sql = "SELECT `login` FROM `users` WHERE id = :id";
    $sth = $db->prepare($sql);
    $sth->execute(array('id' => $uid));
    $username = $sth->fetch(PDO::FETCH_ASSOC);
    if ($username)
        return $username['login'];
    return ("not found id:" . $uid);
}
