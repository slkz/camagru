<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 09/06/2018
 * Time: 01:03
 */

function base64_to_png($base64_string, $output_file) {
    // open the output file for writing
    $ifp = fopen($output_file, 'wb');

    // we could add validation here with ensuring count( $data ) > 1
    fwrite($ifp, base64_decode($base64_string));

    // clean up the file resource
    fclose($ifp);

    return $output_file;
}

function is_user_owner($db, $eid, $uid) {
    $sql = "SELECT COUNT(id) FROM edit WHERE uid = :uid && id = :eid";
    $sth = $db->prepare($sql);
    $sth->bindParam(':uid', $uid);
    $sth->bindParam(':eid', $eid);
    $sth->execute();
    $res = $sth->fetch();
    if ($res['COUNT(id)'] == 1)
        return 1;
    else
        return 0;
}

function jsRedirectAlert ($url, $alert) {
    if (!empty($alert))
        echo '<script language="javascript">alert("' . "$alert" . '")</script>';
    echo '<script language="javascript">window.location.href ="'.$url.'"</script>';
}
