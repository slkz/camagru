<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 09/06/2018
 * Time: 04:40
 */

session_start();

require_once "lib/usersh.php";
require_once 'lib/lib.php';
require_once 'config/database.php';

function edit_mail($db, $newmail, $confirm, $passwd)
{
    $login = session_get("logged_on_user");
    if ($newmail !== $confirm || auth($db, login, $passwd) === FALSE)
        return (-1);
    $sql = "UPDATE users SET mail = :newmail WHERE login = :login";
    $sth = $db->prepare($sql);
    if (!$sth->execute(array(':newmail' => $newmail, ':login' => $login)))
        return (-1);
    return (0);
}

$db = $DB;
if (isset($_SESSION['logged_on_user']) && isset($_POST['newmail']) && isset($_POST['confirm'])
    && isset($_POST['passwd']) && isset($_POST['submit'])) {
    if ($_POST["submit"] === 'OK' && $_SESSION["logged_on_user"] && $_POST["newmail"] && $_POST["confirm"]
        && $_POST["passwd"]) {
        if (mail_exist($db, $newmail))
            jsRedirectAlert("edit_mail.html", "NEW MAIL ALREADY USED");
        else if (edit_mail($db, $_POST["newmail"], $_POST["confirm"], $_POST["passwd"]) === -1)
            jsRedirectAlert("routes/home.php", "ERROR");
        else
            jsRedirectAlert("routes/home.php", "Your mail has been successfully updated");
    }
    else
        jsRedirectAlert("edit_mail.html", "ERROR | INVALID PASSWORD");
}
else {
    include_once "includes/header.php";
    include_once "edit_mail.html";
    include_once "includes/footer.php";
}
