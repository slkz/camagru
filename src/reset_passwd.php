<?php
session_start();
include_once "includes/header.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Edit Password</title>
</head>
<body>
<form method="post" action="reset_passwd.php">
    Account email: <input type="email" name="mail" value="" />
    <br />
    <input type="submit" name="submit" value="OK" />
</form>
</body>
</html>

<?php
/**
 * Created by PhpStorm.
 * User: lucuzzuc
 * Date: 9/5/18
 * Time: 9:33 PM
 */

include_once "includes/footer.php";
require_once "lib/lib.php";
require_once "config/database.php";

$db = $DB;

function send_resetPasswd($toEmail, $newPasswd) {

    $content = "Someone ask to reset your password.\nYou can now log in with this password: " . $newPasswd . "\n";
    $headers = array();
    $headers[] = "From: Camagru <no-reply@camagru.com>";
    if (mail($toEmail, "Your new password", $content, implode($headers, "\r\n")))
        return 1;
    return 0;

}

function reset_passwd($db, $mail) {
    $generatedPasswd = bin2hex(openssl_random_pseudo_bytes(4));
    $generatedHash = hash("whirlpool", $generatedPasswd);
    $sql = "UPDATE `users` SET `passwd` = :newpasswd WHERE `mail` = :mail";
    $sth = $db->prepare($sql);
    if ($sth->execute(array(':newpasswd' => $generatedHash, 'mail' => $mail))) {
        send_resetPasswd($mail, $generatedPasswd);
    }
}

if (isset($_POST['mail']) && isset($_POST['submit'])) {
    if ($_POST['submit'] === 'OK') {
        reset_passwd($db, $_POST['mail']);
        jsRedirectAlert("routes/home.php", "A new password has been sent to the specified email if existing");
    }
}