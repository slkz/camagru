// Prefer camera resolution nearest to 1280x720.
var constraints = { audio: false, video: { width: 1280, height: 720 } };

// name html elements
var video = document.querySelector('#camera-stream'),
    image = document.querySelector('#snap'),
    take_snap_btn = document.querySelector('#take-snap'),
    delete_snap_btn = document.querySelector('#delete-snap'),
    save_snap_btn = document.querySelector('#save-snap'),
    upload_btn = document.querySelector('#upload'),
    upload_file = document.getElementById('upload-file'),
    controls = document.querySelector('.controls');


navigator.mediaDevices.getUserMedia(constraints)
    .then(function(mediaStream) {
        video.srcObject = mediaStream;
        video.onloadedmetadata = function(e) {
            video.play();
            showVideo();
        };
    })
    .catch(function(err) {
        console.log(err.name + ": " + err.message); }); // always check for errors at the end.

function take_snapshot() {
    var hidden = document.querySelector('canvas'),
        context = hidden.getContext('2d'),
        width = video.videoWidth,
        height = video.videoHeight;
    if (width && height) {
        hidden.width = width;
        hidden.height = height;
        context.drawImage(video, 0, 0, width, height);
        return hidden.toDataURL('image/png');
    }
}

upload_btn.addEventListener("click", function (e) {
    e.preventDefault();

    var file = upload_file['files'][0],
        fd = new FormData(),
        overlayId = document.querySelector('input[name="overlay"]:checked').value;
    fd.append('upload_file', file);
    fd.append('overlayId', overlayId);

    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'http://192.168.99.100/capture.php');
    xhr.send(fd);
    xhr.addEventListener('readystatechange', function () {
       if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
           // formattage de la reponse
           var response = JSON.parse(xhr.responseText);

           // acces a la valeur de 'data:'
           var edit = response['data'];

           // ajout prefix base 64 png
           edit = 'data:image/png;base64,' + edit;
           // Show snapshot
           image.setAttribute('src', edit);
           image.classList.add("visible");


           // Enable delete and save buttons
           controls.classList.add('visible');
           delete_snap_btn.classList.remove("disabled");
           save_snap_btn.classList.remove("disabled");
           take_snap_btn.classList.add("disabled");
           video.pause();
           video.classList.remove('visible');
       }
    })
});

take_snap_btn.addEventListener("click", function (e) {
    e.preventDefault();
    var snap = take_snapshot();
    var xhr = new XMLHttpRequest();
    var overlayId = document.querySelector('input[name="overlay"]:checked').value;

    xhr.open('POST', 'http://192.168.99.100/capture.php');
    snap = encodeURIComponent(snap.split(',')[1]);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("snap=" + snap + "&overlayId=" + overlayId);
    xhr.addEventListener('readystatechange', function() {
        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
            // formattage de la reponse
            var response = JSON.parse(xhr.responseText);

            // acces a la valeur de 'data:'
            var edit = response["data"];

            // ajout prefix base 64 png
            edit = 'data:image/png;base64,' + edit;
            // Show snapshot
            image.setAttribute('src', edit);
            image.classList.add("visible");

            // Enable delete and save buttons
            delete_snap_btn.classList.remove("disabled");
            save_snap_btn.classList.remove("disabled");
            take_snap_btn.classList.add("disabled");
            video.pause();
        }
    });
});

save_snap_btn.addEventListener("click", function (e) {
    e.preventDefault();

    var encoded = image.getAttribute('src');
    var xhr = new XMLHttpRequest();

    xhr.open('POST', 'http://192.168.99.100/save.php');
    encoded = encodeURIComponent(encoded.split(',')[1]);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("enc=" + encoded);
    xhr.addEventListener('readystatechange', function (){
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                // Hide snapshot
                image.setAttribute('src', "");
                image.classList.remove("visible");

                // Disable delete and save buttons
                delete_snap_btn.classList.add("disabled");
                save_snap_btn.classList.add("disabled");
                take_snap_btn.classList.remove("disabled");
                video.classList.add("visible");

                video.play();
            }
            else
                alert(xhr.statusText);
        }
    });
});

delete_snap_btn.addEventListener("click", function (e) {
    e.preventDefault();

    // Hide snapshot
    image.setAttribute('src', "");
    image.classList.remove("visible");

    // Disable delete and save buttons
    delete_snap_btn.classList.add("disabled");
    save_snap_btn.classList.add("disabled");
    take_snap_btn.classList.remove("disabled");
    video.classList.add('visible');

    video.play();
});

function showVideo() {
    hideUI();
    video.classList.add("visible");
    controls.classList.add("visible");
}

function hideUI() {
    controls.classList.remove("visible");
    video.classList.remove("visible");
    image.classList.remove("visible");
}