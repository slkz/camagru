<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 09/06/2018
 * Time: 04:03
 */

session_start();

require_once "lib/usersh.php";
require_once "lib/lib.php";
require_once "config/database.php";

function edit_passwd($db, $oldpw, $newpw, $confirm)
{
    $login = session_get("logged_on_user");
    if ($newpw !== $confirm || auth($db, $login, $oldpw) === FALSE)
        return (-1);
    $hash = hash("whirlpool", $newpw);
    $hash2 = hash("whirlpool", $oldpw);
    $sql = "UPDATE users SET passwd = :newpw WHERE login = :login && passwd = :oldpw";
    $sth = $db->prepare($sql);
    if (!$sth->execute(array(':newpw' => $hash, ':login' => $login, ':oldpw' => $hash2)))
        return (-1);
    return (0);
}

$db = $DB;
if (isset($_SESSION["logged_on_user"]) && isset($_POST['oldpw']) && isset($_POST['newpw']) && isset($_POST['confirm'])
    && isset($_POST['submit'])) {
    if ($_POST["submit"] === 'OK' && $_SESSION["logged_on_user"] && $_POST["oldpw"] && $_POST["newpw"]
        && $_POST["confirm"]) {
        if (!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{4,50}$/', $_POST['newpw'])) {
            jsRedirectAlert('edit_passwd.html', 'The password does not meet the requirements!');
            exit ;
        }
        if (edit_passwd($db, $_POST["oldpw"], $_POST["newpw"], $_POST["confirm"]) === -1)
            jsRedirectAlert("routes/home.php", "ERROR | INVALID PASSWORD");
        else
            jsRedirectAlert("routes/home.php", "Your password has been successfully updated");
    }
    else
        jsRedirectAlert("edit_passwd.html","ERROR | CONFIRM PASSWORD DIDNT MATCH");
}
else {
    include_once "includes/header.php";
    include_once "edit_login.html";
    include_once "includes/footer.php";
}
