<?php
/**
 * Created by PhpStorm.
 * User: lucuzzuc
 * Date: 9/18/18
 * Time: 4:32 PM
 */

session_start();
require_once "config/database.php";
require_once "lib/lib.php";

$db = $DB;
if (isset($_SESSION['logged_on_user'])) {
    $sql = "UPDATE `users` SET `notification` = 0 WHERE `login` = :login && `notification` = 1";
    $sth = $db->prepare($sql);
    $sth->execute(array(':login' =>  $_SESSION['logged_on_user']));
    if ($sth->rowCount())
        jsRedirectAlert('./edit_mail.php', '');
    else {
        $sql = "UPDATE `users` SET `notification` = 1 WHERE `login` = :login && `notification` = 0";
        $sth = $db->prepare($sql);
        $sth->execute(array(':login' => $_SESSION['logged_on_user']));
        if ($sth->rowCount())
            jsRedirectAlert('./edit_mail.php', '');
    }
    jsRedirectAlert('./edit_mail.php', '');
}