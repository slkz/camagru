<?php
/**
 * Created by PhpStorm.
 * User: lucuzzuc
 * Date: 9/5/18
 * Time: 8:54 PM
 */

session_start();

require_once "lib/usersh.php";
require_once "lib/lib.php";
require_once "config/database.php";

$db = $DB;

if (isset($_GET['code'])) {
    if (confirm_account($db, $_GET['code']) === TRUE)
        jsRedirectAlert("routes/login.php","Account successfully confirmed, you can now log in");
    jsRedirectAlert("routes/login.php","ERROR | INVALID CODE");
}