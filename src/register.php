<?php

session_start();

require_once "lib/usersh.php";
require_once 'lib/lib.php';
require_once 'config/database.php';

function create_account($db, $login, $passwd, $mail, $activateCode)
{
    if (login_exist($db, $login) || mail_exist($db, $mail)) {
        return (-1);
    }
    else {
        $sql = "INSERT INTO users (login, passwd, mail, activateCode) VALUES (:login, :passwd, :mail, :activateCode)";
        $sth = $db->prepare($sql);
        $hash = hash("whirlpool", $passwd);
        $sth->execute(array(':login' => $login, ':passwd' => $hash, ':mail' => $mail, ':activateCode' => $activateCode));
    }
}

function send_confirmationEmail($toEmail, $activateCode) {

    $link = "http://$_SERVER[HTTP_HOST]"."/activate.php?code=".$activateCode;
    $content = "Welcome to Camagru.\nPlease, click on this link to activate your account.\n" . $link;
    $headers = array();
    $headers[] = "From: Camagru <no-reply@camagru.com>";
    if (mail($toEmail, "Welcome", $content, implode($headers, "\r\n")))
        return 1;
    return 0;

}

$db = $DB;
if (isset($_POST['login']) && isset($_POST['passwd']) && isset($_POST['mail']) && isset($_POST['submit'])){
    if ($_POST["submit"] === 'OK' && $_POST["login"] && $_POST["passwd"] && $_POST["mail"]) {
        $activateCode = bin2hex(openssl_random_pseudo_bytes(8));
        if (!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{4,50}$/', $_POST['passwd'])) {
            jsRedirectAlert('routes/register.php', 'The password does not meet the requirements!');
            exit ;
        }
        if (create_account($db, $_POST["login"], $_POST["passwd"], $_POST["mail"], $activateCode) === -1)
            jsRedirectAlert("routes/register.php", "ERROR | LOGIN OR MAIL ALREADY USED");
        if (send_confirmationEmail($_POST['mail'], $activateCode)) {
          header('Location: /');
          echo "Thank you for registering with us, check you email to activate your account.\n";
          exit ;
        }
        else
            echo "CANNOT SEND THE CONFIRMATION MAIL\n";
        jsRedirectAlert("routes/home.php", "Registration done, please verify your mails");
    }
    else
        jsRedirectAlert("routes/home.php", "ERROR | INVALID INSERT IN LOGIN OR PASSWD");
}
else
    jsRedirectAlert("routes/register.php","ERROR | Empty Field");
