<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 11/06/2018
 * Time: 16:27
 */

session_start();

require_once "lib/lib.php";

function trick_pro_php($obj){
    // extract content of an object and convert it to base 64
    ob_start();
    imagepng($obj);
    $image_data = ob_get_contents();
    ob_end_clean();
    return(base64_encode($image_data));
}

function img_to_resource($file){
    switch ($file['type']){
        case "image/png":
            $resource = imagecreatefrompng($file['tmp_name']);
            break;
        case "image/jpeg":
            $resource = imagecreatefromjpeg($file['tmp_name']);
            break;
        case "image/gif":
            $resource = imagecreatefromgif($file['tmp_name']);
            break;
        default:
            $resource = NULL;
    }
    return($resource);
}

$tmp_path =  "/tmp/".$_SESSION['logged_on_user']."_".date("mdY_his").'.png';
$overlayId = $_POST['overlayId'];
$overlay_path = "img/" . $overlayId . ".png";

// converting png to resources
$overlay = imagecreatefrompng($overlay_path);

if (isset($_POST['snap'])){
    $edit = imagecreatefrompng(base64_to_png($_POST['snap'], $tmp_path));
    $image_size = getimagesize($tmp_path);
}

if (isset($_FILES['upload_file'])){
    $edit = img_to_resource($_FILES['upload_file']);
    $image_size = getimagesize($_FILES['upload_file']['tmp_name']);
}

// make the edit
imagecopyresampled($edit, $overlay, 0, 0, 0, 0, getimagesize($overlay_path)[0], getimagesize($overlay_path)[1], getimagesize($overlay_path)[0], getimagesize($overlay_path)[1]);

// transform the resource to base64
$base_64 = trick_pro_php($edit); // prends resource donne base64
// save the edited png
base64_to_png($base_64, $tmp_path); // save base64 dans tmppath

header('Content-Type: application/json');
echo (json_encode(array('data' => $base_64)));
