<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 09/06/2018
 * Time: 03:29
 */

session_start();

require_once 'lib/usersh.php';
require_once 'lib/lib.php';
require_once 'config/database.php';

$db = $DB;

if (isset($_POST['login']) && isset($_POST['passwd'])) {
    if (($uid = auth($db, $_POST['login'], $_POST['passwd']))) {
        session_add("logged_on_user", $_POST['login']);
        session_add("uid", $uid);
        header('Location: routes/home.php');
    }
    else {
        logout();
        jsRedirectAlert("routes/login.php","ERROR | INVALID LOGIN OR PASSWORD");
    }
}
else {
    logout();
    jsRedirectAlert("routes/login.php","ERROR");
}
