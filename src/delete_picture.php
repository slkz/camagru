<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 07/07/2018
 * Time: 14:19
 */

require_once "lib/lib.php";
require_once "config/database.php";

session_start();

$db = $DB;
if (isset($_SESSION['logged_on_user'])) {
    $uid = $_SESSION['uid'];
    if (isset($_GET['id'])) {
        $eid = $_GET['id'];
        $picture_url = "routes/picture.php?id=" . $eid;
        // check if the user is the owner
        if (is_user_owner($db, $eid, $uid) === 1) {
           // delete likes
            $sql = "DELETE FROM likes WHERE eid = :eid";
            $sth = $db->prepare($sql);
            $sth->bindParam(':eid', $eid);
            if ($sth->execute()) {
                // delete comments
                $sql = "DELETE FROM comments WHERE eid = :eid";
                $sth = $db->prepare($sql);
                $sth->bindParam(':eid', $eid);
                if ($sth->execute()) {
                   // delete picture
                    $sql = "DELETE FROM edit WHERE uid = :uid && id = :eid";
                    $sth = $db->prepare($sql);
                    $sth->bindParam(':uid', $uid);
                    $sth->bindParam(':eid', $eid);
                    if ($sth->execute())
                        jsRedirectAlert("routes/home.php", "Picture successfully deleted");
                }
            }

        }
        else
            jsRedirectAlert($picture_url, "ERROR | You must be the owner of the picture to delete it");
    }
}
