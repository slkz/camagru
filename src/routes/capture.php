<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 23/06/2018
 * Time: 00:16
 */

session_start();
include_once "../includes/header.php";
?>
    <div class="columns is-mobile">
        <div class="column is-three-quarters">
            <div class="app">
                <video id="camera-stream"></video>
                <img id="snap">

                <div class="controls">
                    <a href="#" id="delete-snap" title="Delete It!" class="disabled">Delete</a>
                    <a href="#" id="take-snap" title="Snap It!">Snap</a>
                    <a href="#" id="save-snap" title="Save It!" class="disabled">Save</a>
                </div>
                <canvas></canvas>
            </div>
        </div>
        <div class="column" style="background-color: rgba(136,173,182,0.1)">
            <div style="max-width:150px">
                <input type="radio" id="overlay1" name="overlay" value="1" checked>
                <label for="overlay1">
                <figure class="image is-square">
                    <img src="../img/1.png">
                </figure>
                </label>
            </div>
            <div style="max-width:150px">
                <input type="radio" id="overlay2" name="overlay" value="2">
                <label for="overlay2">
                <figure class="image is-square">
                    <img alt="overlay2" src="../img/2.png">
                </figure>
                </label>
            </div>
            <div style="max-width:150px">
                <input type="radio" id="overlay3" name="overlay" value="3">
                <label for="overlay3">
                <figure class="image is-square">
                    <img src="../img/3.png">
                </figure>
                </label>
            </div>
            <div style="max-width:150px">
                <input type="radio" id="overlay4" name="overlay" value="4">
                <label for="overlay4">
                    <figure class="image is-square">
                        <img src="../img/4.png">
                    </figure>
                </label>
            </div>
            <div style="max-width:150px">
                <input type="radio" id="overlay5" name="overlay" value="5">
                <label for="overlay5">
                    <figure class="image is-square">
                        <img src="../img/5.png">
                    </figure>
                </label>
            </div>
            <div style="max-width:150px">
                <input type="radio" id="overlay6" name="overlay" value="6">
                <label for="overlay6">
                    <figure class="image is-square">
                        <img src="../img/6.png">
                    </figure>
                </label>
            </div>
        </div>
    </div>
    <form method="POST" action="../capture.php" enctype="multipart/form-data">
        <input id="upload-file" type="file" name="uploaded-file">
        <input id="upload" type="button" name="submit" value="OK">
    </form>
    <script src="../capture.js"></script>
<?php

require_once "../config/database.php";

$db = $DB;

// get the total number of img in the galery
$sql = "SELECT COUNT(id) FROM edit WHERE uid = :uid";
$sth = $db->prepare($sql);
$sth->bindParam(':uid', $_SESSION['uid']);
$sth->execute();
$res = $sth->fetch();
$totalImg = $res["COUNT(id)"];

// calcul how much page we will need
$ipp = 5;
$totalPages = ceil($totalImg / $ipp);

function get_user_galery($db, $page, $totalPages, $ipp) {

    // get image for selected page
    $offset = ($page - 1) * $ipp;
    $sql = "SELECT id, path FROM edit WHERE uid = :uid LIMIT :offset,:ipp";
    $sth = $db->prepare($sql);
    $sth->bindParam(':offset', $offset, PDO::PARAM_INT);
    $sth->bindParam(':ipp', $ipp, PDO::PARAM_INT);
    $sth->bindParam(':uid', $_SESSION['uid']);
    $sth->execute();
    $res = $sth->fetchAll(PDO::FETCH_ASSOC);
    echo "<div class='galery-row'>";
    foreach ($res as $img) {
        echo "
                <div class='galery-column'>
                    <a href = 'picture.php?id=" . $img['id'] . "'>
                    <img src='../" . $img['path'] . "' style='width:100%'>
                    </a>
                </div>";
    }
    echo "</div>";
    // Paginate
    $i = 1;
    echo "
        <div class='has-text-centered'>";
    while ($i <= $totalPages) {
        echo "
            <a href='capture.php?page=" . $i . "'>" . $i . "</a>";
        $i++;
    }
    echo "
        </div>";
}

if (isset($_GET['page']) && $_GET['page'] > 1 && $_GET['page'] <= $totalPages)
    get_user_galery($db, $_GET['page'], $totalPages, $ipp);
else
    get_user_galery($db, 1, $totalPages, $ipp);

include_once "../includes/footer.php";