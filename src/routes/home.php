<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 23/06/2018
 * Time: 00:16
 */

session_start();
include_once "../includes/header.php";
require_once "../config/database.php";

$db = $DB;

// get the total number of img in the galery
$sql = "SELECT COUNT(id) FROM edit";
$sth = $db->prepare($sql);
$sth->execute();
$res = $sth->fetch();
$totalImg = $res["COUNT(id)"];

// calcul how much page we will need
$ipp = 5;
$totalPages = ceil($totalImg / $ipp);

function get_picture_galery($db, $page, $totalPages, $ipp) {

    // get image for selected page
    $offset = ($page - 1) * $ipp;
    $sql = "SELECT id, path FROM edit LIMIT :offset,:ipp";
    $sth = $db->prepare($sql);
    $sth->bindParam(':offset', $offset, PDO::PARAM_INT);
    $sth->bindParam(':ipp', $ipp, PDO::PARAM_INT);
    $sth->execute();
    $res = $sth->fetchAll(PDO::FETCH_ASSOC);
    echo "<div class='galery-row'>";
    foreach ($res as $img) {
        echo "
                <div class='galery-column'>
                    <a href = 'picture.php?id=" . $img['id'] . "'>
                    <img src='../" . $img['path'] . "' style='width:100%'>
                    </a>
                </div>";
    }
    echo "</div>";
    // Paginate
    $i = 1;
    echo "
        <div class='has-text-centered'>";
    while ($i <= $totalPages) {
        echo "
            <a href='home.php?page=" . $i . "'>" . $i . "</a>";
        $i++;
    }
    echo "
        </div>";
}

if (isset($_GET['page']) && $_GET['page'] > 1 && $_GET['page'] <= $totalPages)
    get_picture_galery($db, $_GET['page'], $totalPages, $ipp);
else
    get_picture_galery($db, 1, $totalPages, $ipp);

include_once "../includes/footer.php";