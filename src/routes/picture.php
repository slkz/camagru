<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 24/06/2018
 * Time: 20:36
 */

session_start();
include_once "../includes/header.php";
require_once "../config/database.php";
require_once "../lib/lib.php";
require_once "../lib/usersh.php";

$db = $DB;

function display_comments($db, $eid, $page, $totalPages, $cpp) {
    // Display comments
    $offset = ($page - 1) * $cpp;
    $sql = "SELECT text, date, uid FROM comments WHERE eid = :eid LIMIT :offset,:cpp";
    $sth = $db->prepare($sql);
    $sth->bindParam(':eid', $eid, PDO::PARAM_INT);
    $sth->bindParam(':offset', $offset, PDO::PARAM_INT);
    $sth->bindParam(':cpp', $cpp, PDO::PARAM_INT);
    $sth->execute();
    $res = $sth->fetchAll(PDO::FETCH_ASSOC);
    echo "
        <div class='column has-text-centered'>";
    foreach ($res as $cmt) {
        $username = get_username($db, $cmt['uid']);
        echo "
            <div class='cmt'>
                <div class='cmtText'>" . htmlspecialchars($cmt['text']) . "</div>
                <div class='cmtAuth'>" . htmlspecialchars($username) . "</div>
                <div class='cmtDate'>" . $cmt['date'] . "</div>
            </div> 
        ";
    }
    // post comment div
    echo "
            <div>
                <form method='post' action='../send_comment.php'>
                    Your Comment: <textarea class='textarea' name='comment' placeholder='your comment'></textarea>
                    <input type='submit' name='submit' value='OK'/>
                    <input hidden type='text' name='eid' value='" . $eid . "'/>
                </form>
            </div>";
    // Paginate comments
    $i = 1;
    while ($i <= $totalPages) {
        echo "<a href='picture.php?id=" . $eid . "&page=" . $i . "'>" . $i . "</a>";
        $i++;
    }
    echo "
        </div>
    </div>";
}

if (isset($_GET['id'])) {
    // check if the id img exist and get path
    $imgID = $_GET['id'];
    $sql = "SELECT path FROM edit WHERE id = :id";
    $sth = $db->prepare($sql);
    $sth->execute(array(':id' => $imgID));
    $res = $sth->fetch(PDO::FETCH_ASSOC);
    if (empty($res)) {
        jsRedirectAlert('home.php', "");
        exit();
    }
    // display img on screen and add delete option
    echo "
        <div class='columns is-mobile'>
            <div class='column is-two-thirds'>
                <img class='picture' id='" . $imgID . "' src='../" . $res['path'] . "' style='width:100%'>";
    if (is_user_owner($db, $imgID, $_SESSION['uid']) === 1)
        echo "
                <a href='../delete_picture.php?id=" . $imgID . "'>Delete</a>";
    // display like and like btn
    $sql = "SELECT COUNT(id) FROM likes WHERE eid = :eid";
    $sth = $db->prepare($sql);
    $sth->bindParam(":eid", $imgID, PDO::PARAM_INT);
    $sth->execute();
    $res = $sth->fetch(PDO::FETCH_ASSOC);
    $like_count = $res['COUNT(id)'];
    echo "
                <div class='like-section'>
                    <span id='like-count'>" . $like_count . "</span> peoples like this!
                    <button type='button' id='like-btn'>like!</button>
                    <script src='../like.js'></script>
                </div> 
            </div>";
    // display comment if there is
    $sql = "SELECT COUNT(id) FROM comments WHERE eid = :eid";
    $sth = $db->prepare($sql);
    $sth->bindParam(':eid', $imgID, PDO::PARAM_INT);
    $sth->execute();
    $res = $sth->fetch(PDO::FETCH_ASSOC);
    $totalCmt = $res['COUNT(id)'];

    $cpp = 5;
    $totalPages = ceil($totalCmt / $cpp);
    // display img comments
    if (isset($_GET['page']) && $_GET['page'] >= 1 && $_GET['page'] <= $totalPages)
        display_comments($db, $imgID, $_GET['page'], $totalPages, $cpp);
    else
        display_comments($db, $imgID, 1, $totalPages, $cpp);

}
else
    header("Location: home.php");

include_once "../includes/footer.php";
