<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 23/06/2018
 * Time: 01:06
 */

session_start();

require_once "../lib/usersh.php";

logout();
header('Location: ./home.php');