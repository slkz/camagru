<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 09/06/2018
 * Time: 04:26
 */

session_start();

require_once "lib/usersh.php";
require_once 'lib/lib.php';
require_once 'config/database.php';

function edit_login($db, $newid, $passwd, $confirm)
{
    $login = session_get("logged_on_user");
    if ($newid !== $confirm || auth($db, $login, $passwd) === FALSE)
        return (-1);
    $sql = "UPDATE users SET login = :newid WHERE login = :login";
    $sth = $db->prepare($sql);
    if (!$sth->execute(array(':newid' => $newid, ':login' => $login)))
        return (-1);
    return (0);
}


$db = $DB;
if (isset($_SESSION['logged_on_user']) && isset($_POST['newid']) && isset($_POST['confirm'])
    && isset($_POST['passwd']) && isset($_POST['submit'])) {
    if ($_POST["submit"] === 'OK' && $_SESSION["logged_on_user"] && $_POST["newid"] && $_POST["confirm"]
        && $_POST["passwd"]) {
        if (login_exist($db, $newid))
            jsRedirectAlert("edit_login.html","NEW LOGIN ALREADY TAKEN");
        else if (edit_login($db, $_POST["newid"], $_POST["passwd"], $_POST["confirm"]) === -1)
            jsRedirectAlert("routes/home.php", "ERROR");
        else {
            $_SESSION['logged_on_user'] = $_POST['newid'];
            print_r($_SESSION);
            jsRedirectAlert("routes/home.php", "Your login has been successfully updated");
        }
    }
    else
        jsRedirectAlert("edit_login.html","ERROR | INVALID PASSWORD");
}
else {
    include_once "includes/header.php";
    include_once "edit_login.html";
    include_once "includes/footer.php";
}
