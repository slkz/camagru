var like_btn = document.querySelector('#like-btn'),
    imgID = document.querySelector('.picture'),
    like_count = document.getElementById('like-count'),
    eid = imgID.getAttribute('id');

like_btn.addEventListener("click", function (e) {
   e.preventDefault();

   var xhr = new XMLHttpRequest();

   xhr.open('POST', 'http://192.168.99.100/like.php');
   xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
   xhr.send("eid=" + eid);
   xhr.addEventListener('readystatechange', function () {
      if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
              // format. reponse
              var response = JSON.parse(xhr.responseText);
              var action = response['done'];
              if (action === 'like')
                  like_count.innerHTML = (parseInt(like_count.innerHTML) + 1).toString();
              else if (action === 'unlike')
                  like_count.innerHTML = (parseInt(like_count.innerHTML) - 1).toString();
              else
                  location.href = "login.php";
          }
   })
});