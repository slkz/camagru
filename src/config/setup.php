<?php
/**
 * Created by PhpStorm.
 * User: lucuzzuc
 * Date: 9/12/18
 * Time: 6:44 PM
 */

require_once 'database.php';

function createTables($db) {

// delete if exists
    $sql = "
DROP TABLE IF EXISTS `comments`;
DROP TABLE IF EXISTS `likes`;
DROP TABLE IF EXISTS `edit`;
DROP TABLE IF EXISTS `users`;";
    $sth = $db->prepare($sql);
    if ($sth->execute() === FALSE)
        echo "cannot drop";

// comments table
    $sql = "
CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `text` text NOT NULL,
  `date` datetime NOT NULL,
  `uid` int(11) NOT NULL,
  `eid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1";
    $sth = $db->prepare($sql);
    if ($sth->execute() === FALSE)
        echo "cannot create comments";

// edit table
    $sql = "
CREATE TABLE `edit` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `path` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1";
    $sth = $db->prepare($sql);
    if ($sth->execute() === FALSE)
        echo "cannot create edit";

// likes table
    $sql = "
CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `eid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1";
    $sth = $db->prepare($sql);
    if ($sth->execute() === FALSE)
        echo "cannot create likes";

// users table
    $sql = "
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `login` varchar(255) NOT NULL,
  `passwd` text NOT NULL,
  `mail` varchar(255) NOT NULL,
  `activateCode` text NOT NULL,
  `confirmed` int(11) NOT NULL DEFAULT '0',
  `notification` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1";
    $sth = $db->prepare($sql);
    if ($sth->execute() === FALSE)
        echo "cannot create users";
    return 1;
};

function createIndexes($db) {
//-- Indexes for table `comments`
    $sql = "
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_eid` (`eid`),
  ADD KEY `FK_usserId` (`uid`);
    ";
    $sth = $db->prepare($sql);
    if ($sth->execute() === FALSE)
        return 0;

//-- Indexes for table `edit`
    $sql = "
ALTER TABLE `edit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_uid` (`uid`);
  ";
    $sth = $db->prepare($sql);
    if ($sth->execute() === FALSE)
        return 0;

//-- Indexes for table `likes`
    $sql = "
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_like` (`uid`,`eid`),
  ADD KEY `FK_editId` (`eid`);
  ";
    $sth = $db->prepare($sql);
    if ($sth->execute() === FALSE)
        return 0;

//-- Indexes for table `users`
    $sql = "
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`),
  ADD UNIQUE KEY `mail` (`mail`);
  ";
    $sth = $db->prepare($sql);
    if ($sth->execute() === FALSE)
        return 0;
    return 1;
};

function createIncrements($db) {
//-- AUTO_INCREMENT for table `comments`
    $sql = "
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT
  ";
    $sth = $db->prepare($sql);
    if ($sth->execute() === FALSE)
        return 0;

//-- AUTO_INCREMENT for table `edit`
    $sql = "
ALTER TABLE `edit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  ";
    $sth = $db->prepare($sql);
    if ($sth->execute() === FALSE)
        return 0;

//-- AUTO_INCREMENT for table `likes`
    $sql = "
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT
  ";
    $sth = $db->prepare($sql);
    if ($sth->execute() === FALSE)
        return 0;

//-- AUTO_INCREMENT for table `users`
    $sql = "
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
  ";
    $sth = $db->prepare($sql);
    if ($sth->execute() === FALSE)
        return 0;
    return 1;
};

function createConstraints($db) {
//-- Constraints for table `comments`
    $sql = "
ALTER TABLE `comments`
  ADD CONSTRAINT `FK_eid` FOREIGN KEY (`eid`) REFERENCES `edit` (`id`),
  ADD CONSTRAINT `FK_usserId` FOREIGN KEY (`uid`) REFERENCES `users` (`id`);
  ";
    $sth = $db->prepare($sql);
    if ($sth->execute() === FALSE)
        return 0;

//-- Constraints for table `edit`
    $sql = "
ALTER TABLE `edit`
  ADD CONSTRAINT `FK_uid` FOREIGN KEY (`uid`) REFERENCES `users` (`id`);
  ";
    $sth = $db->prepare($sql);
    if ($sth->execute() === FALSE)
        return 0;

//-- Constraints for table `likes`
    $sql = "
ALTER TABLE `likes`
  ADD CONSTRAINT `FK_editId` FOREIGN KEY (`eid`) REFERENCES `edit` (`id`),
  ADD CONSTRAINT `FK_userId` FOREIGN KEY (`uid`) REFERENCES `users` (`id`);
  ";
    $sth = $db->prepare($sql);
    if ($sth->execute() === FALSE)
        return 0;
    return 1;
}

if (!createTables($DB))
    echo "error while creating tables";
if (!createIndexes($DB))
    echo "error while creating indexes";
if (!createIncrements($DB))
    echo "error while creating incrementation";
if (!createConstraints($DB))
    echo "error while creating constraints";
else
    echo "Camagru database correctly created";