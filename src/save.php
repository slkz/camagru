<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 22/06/2018
 * Time: 18:16
 */

session_start();

require_once "lib/lib.php";
require_once "config/database.php";
require_once "lib/usersh.php";

if (isset($_SESSION['logged_on_user'])) {
    if (isset($_POST['enc'])) {
        // create unique name, get uid and init sql connexion
        $edit_path = "galery/" . $_SESSION['logged_on_user'] . "_" . date("mdY_his") . '.png';
        $db = $DB;
        // create the file in /galery/
        base64_to_png($_POST['enc'], $edit_path);
        // insert to db
        $sql = "INSERT INTO edit (path, uid) VALUES (:path, :uid)";
        $sth = $db->prepare($sql);
        $sth->execute(array(':path' => $edit_path, ':uid' => $_SESSION['uid']));
    }
    else
        http_response_code(403);
}
else
    http_response_code(401);