<?php
/**
 * Created by PhpStorm.
 * User: Lucas
 * Date: 24/06/2018
 * Time: 17:42
 */

session_start();

require_once "config/database.php";
require_once "lib/lib.php";

function send_notificationEmail($db, $eid) {

    $sql = "SELECT `uid` FROM `edit` WHERE `id` = :id";
    $sth = $db->prepare($sql);
    $sth->bindParam(':id', $eid);
    $sth->execute();
    $res = $sth->fetch(PDO::FETCH_ASSOC);

    $sql = "SELECT `mail` FROM `users` WHERE `id` = :id && `notification` = 1";
    $sth = $db->prepare($sql);
    $sth->bindParam(':id', $res['uid']);
    $sth->execute();
    $toEmail = $sth->fetch();

    if ($toEmail) {
        $content = "Someone comment one of your edits";
        $headers = array();
        $headers[] = "From: Camagru <no-reply@camagru.com>";
        if (mail($toEmail['mail'], "New comment", $content, implode($headers, "\r\n")))
            return 1;
        return 0;
    }
}

if (isset($_POST['submit']) && $_POST['submit'] === 'OK') {
    if (!isset($_SESSION['logged_on_user'])) {
        header("Location: routes/login.php");
        exit();
    }
    if (isset($_POST['comment']) && $_POST['comment'] !== "") {
        $db = $DB;
        $date = date('Y-m-d G:i:s');
        $sql = "INSERT INTO comments (text, date, uid, eid) VALUES (:text, :date, :uid, :eid)";
        $sth = $db->prepare($sql);
        $sth->bindParam(':text', $_POST['comment']);
        $sth->bindParam(':date',$date);
        $sth->bindParam(':uid', $_SESSION['uid'], PDO::PARAM_INT);
        $sth->bindParam(':eid', $_POST['eid'], PDO::PARAM_INT);
        if ($sth->execute() == FALSE)
            echo "ERROR | CANNOT INSERT COMMENT";
        else
            send_notificationEmail($db, $_POST['eid']);
    }
    $header = "Location: routes/picture.php?id=" . $_POST['eid'];
    header($header);
}

