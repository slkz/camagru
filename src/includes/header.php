<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="../camagru.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.css">
</head>
<body>
<nav class="navbar is-transparent">
    <div class="navbar-brand">
        <a class="navbar-item" href="../routes/home.php">
            <img src="https://i.imgur.com/0USNj5N.jpg" alt="Camagru by lucuzzuc" width="112" height="28">
        </a>
    </div>

    <div id="navbarExampleTransparentExample" class="navbar-menu is-active">
        <div class="navbar-start">
            <a class="navbar-item" href="../routes/home.php">
                Home
            </a>
            <?php
            if (isset($_SESSION['logged_on_user']) && $_SESSION['logged_on_user'] !== "") {
                ?>
                <a class="navbar-item" href="../routes/capture.php">
                    Capture
                </a>
                <div class="navbar-item has-dropdown is-hoverable">
                    <div class="navbar-link">
                        Profile
                    </div>
                    <div class="navbar-dropdown is-boxed">
                        <a class="navbar-item" href="../edit_login.php">
                            Edit Login
                        </a>
                        <a class="navbar-item" href="../edit_passwd.php">
                            Edit Password
                        </a>
                        <a class="navbar-item" href="../edit_mail.php">
                            Edit Mail
                        </a>
                    </div>
                </div>
            <?php } ?>
        </div>

        <div class="navbar-end">
            <div class="navbar-item">
                <div class="field is-grouped">
                    <?php
                    if (!isset($_SESSION['logged_on_user']) || $_SESSION['logged_on_user'] === "") {
                        ?>
                        <p class="control">
                            <a class="button is-info" href="../routes/register.php">
                            <span>Register</span>
                            </a>
                        </p>
                    <?php }
                    if (isset($_SESSION['logged_on_user']) && $_SESSION['logged_on_user'] !== "") {
                        ?>
                        <p class="control">
                            <a class="button is-danger" href="../routes/logout.php">
                                <span>Logout</span>
                            </a>
                        </p>
                    <?php }
                    else {
                        ?>
                        <p class="control">
                            <a class="button is-primary" href="../routes/login.php">
                                <span>Login</span>
                            </a>
                        </p>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</nav>